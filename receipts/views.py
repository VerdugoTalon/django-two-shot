from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from .forms import ReceiptForm, ExpenseCategoryForm

# Create your views here.
@login_required
def show_receipt_list(request):
  receipt_list = Receipt.objects.filter(purchaser=request.user)
  context = {
    "receipt_list": receipt_list
  }
  return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
  if request.method == 'POST':
    form = ReceiptForm(request.POST)
    if form.is_valid():
      receipt = form.save(commit=False)
      receipt.purchaser = request.user
      receipt.save()
      return redirect('home')
  else:
    form = ReceiptForm()

  return render(request, 'receipts/create_receipt.html', {'form': form})

@login_required
def show_category_list(request):
  category_list = ExpenseCategory.objects.filter(owner=request.user)
  category_data = []
  for category in category_list:
    num_receipts = Receipt.objects.filter(category=category).count()
    category_data.append({'name': category.name, 'num_receipts': num_receipts})
  return render(request, 'receipts/categories.html', {'category_list':category_data})

@login_required
def show_account_list(request):
  account_list = Account.objects.filter(owner=request.user)
  print('This is account list', account_list)
  account_data = []

  for account in account_list:
    num_receipts = Receipt.objects.filter(account=account).count()
    account_data.append({'name': account.name, 'number':account.number, 'num_receipts': num_receipts,})
  print('This is account data', account_data)

  return render(request, 'receipts/accounts.html', {'account_list':account_data} ) 

@login_required
def create_expense_category(request):
  if request.method == "POST":
    form = ExpenseCategoryForm(request.POST)
    if form.is_valid():
      expense_category = form.save(commit=False)
      expense_category.owner = request.user
      expense_category.save()
    
      return redirect('category_list')

  else:
    form = ExpenseCategoryForm()

  context = {
    "form": form
  }

  return render(request, 'receipts/create.html', context)
