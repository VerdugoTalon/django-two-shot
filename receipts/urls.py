from django.urls import path
from .views import show_receipt_list, create_receipt, show_category_list, show_account_list, create_expense_category

urlpatterns = [
    path('categories/create/', create_expense_category, name='create_category'),
    path('accounts/', show_account_list, name='account_list'),
    path('categories/', show_category_list, name='category_list'),
    path('create/', create_receipt, name='create_receipt'),
    path('receipts/', show_receipt_list, name='receipt_list'),
    path('',  show_receipt_list, name='home'),
    ]
