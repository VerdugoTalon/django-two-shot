from django.urls import path
from .views import login_attempt, logout_attempt, create_account

urlpatterns = [
    path('signup/', create_account, name='signup'),
    path('logout/', logout_attempt, name='logout'),
    path('login/', login_attempt, name='login'),
]
